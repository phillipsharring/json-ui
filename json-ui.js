const Ui = {
  config: null,

  errors: {},

  defaultMessages: {
    required: {
      msg: 'Please enter a value for the %s field',
    },
    in: {
      msg: 'Please select a valid choice from: %s',
      replacement: 'rules',
    },
  },

  hasError: function (field = '') {
    if (field === '') {
      return this.errors.length > 0;
    } else {
      return this.errors.find((error) => error.name === field) !== undefined;
    }
  },

  getError: function (field) {
    return this.errors.find((error) => error.name === field);
  },

  getMessage: function (field) {
    const error = this.getError(field);
    return error?.msg;
  },

  addError: function ({ field, type, rules }) {
    this.errors[field.name] = this.formatError(
      field.messages?.[type] ??
        this.defaultMessages[type]?.msg ??
        'Please enter a valid value for %s',
      arguments[0][this.defaultMessages[type]?.replacement] ?? field['name']
    );
  },

  formatError: function (string, replacement) {
    return string.replace('%s', replacement);
  },

  validations: {
    required: function (isRequired, value) {
      return isRequired ? value !== '' : true;
    },
    in: function (list, value) {
      return list.includes(value);
    },
  },

  initConfig: async function () {
    return await fetch('./form.json')
      .then((response) => {
        return response.json();
      })
      .then((data) => data);
  },

  getConfig: async function () {
    if (this.config === null) {
      this.config = await this.initConfig();
    }

    return this.config;
  },

  getFieldId: function (field) {
    return field.id ?? field.name;
  },

  getFieldValue: function (field) {
    const input = document.querySelector('#' + this.getFieldId(field));

    if (field.type === 'select') {
      return input.options[input.selectedIndex].value;
    }

    return input.value;
  },

  createLabel: function (field) {
    const label = document.createElement('label');
    label.setAttribute('for', this.getFieldId(field));
    label.innerText = field.label;
    return label;
  },

  createTextInput: function (field) {
    const input = document.createElement('input');

    input.classList.add('form-input');
    input.setAttribute('type', field.type);
    input.setAttribute('name', field.name);

    return input;
  },

  createSelectInput: function (field) {
    const select = document.createElement('select');

    select.classList.add('form-input');
    select.setAttribute('type', field.type);
    select.setAttribute('name', field.name);

    if (field.emptyOption) {
      const emptyOption = document.createElement('option');
      emptyOption.setAttribute('value', '');
      select.appendChild(emptyOption);
    }

    field.options.forEach((opt) => {
      const option = document.createElement('option');
      option.setAttribute('value', opt.value);
      option.innerText = opt.label;
      select.appendChild(option);
    });

    return select;
  },

  createInput: function (field) {
    let input;

    switch (field.type) {
      case 'text':
        input = this.createTextInput(field);
        break;
      case 'select':
        input = this.createSelectInput(field);
        break;
    }

    input.setAttribute('id', this.getFieldId(field));

    if (false && field.validations?.required) {
      input.setAttribute('required', field.validations.required);
    }

    return input;
  },

  createHelp: function (field) {
    let help = document.createTextNode('');

    if (field.help) {
      help = document.createElement('span');
      help.innerText = field.help;
      help.classList.add('help');
    }

    return help;
  },

  createFieldGroup: function (field) {
    const div = document.createElement('div');
    div.classList.add('field-group');

    div.appendChild(this.createLabel(field));
    div.appendChild(this.createInput(field));
    div.appendChild(this.createHelp(field));

    return div;
  },

  runValidation: function (type, rules, value) {
    let valid = true;

    if (
      this.validations[type] &&
      typeof this.validations[type] === 'function'
    ) {
      valid = this.validations[type](rules, value);
    } else if (typeof rules === 'function') {
      valid = rules(value);
    }

    return valid;
  },

  validateField: function (field) {
    const value = this.getFieldValue(field);
    let valid = true;

    Object.entries(field.validations).forEach(([type, rules]) => {
      v = this.runValidation(type, rules, value);

      if (!v) {
        this.addError({ field, type, rules });
      }

      valid = valid && v;
    });

    return valid;
  },

  submitForm: async function (event) {
    event.preventDefault();
    const form = event.target;

    const config = await this.getConfig();
    let valid = true;

    config.fields.forEach((field) => {
      valid = valid && this.validateField(field);
    });

    if (!valid) {
      const pre = document.createElement('pre');
      pre.innerText = JSON.stringify(this.errors);
      form.appendChild(pre);
      return;
    }

    form.submit();
  },

  initJsonUi: async function () {
    const config = await this.getConfig();

    const ui = document.querySelector('#ui');

    config.fields.forEach((field) => {
      ui.appendChild(this.createFieldGroup(field));
    });

    const form = ui.closest('form');

    form.addEventListener('submit', (event) => {
      this.submitForm(event);
    });
  },
};

window.addEventListener('load', () => {
  Ui.initJsonUi();
});
